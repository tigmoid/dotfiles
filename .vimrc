"Tim Whitehead June 4, 2009
"Pulled from various people's on www.dotfiles.com and Vim help

"We want to use vim not vi
set nocp

" run tpope's pathogen plugin
execute pathogen#infect()

set wildmenu " turn on wild menu
set showmatch " show matching brackets
set mat=6 " how many tenths of a second to blink matching brackets

"
set selectmode=mouse
"use mouse for everything
set mouse=a
"Ignore case in searches
set ignorecase
" but be smart about it when there are capital letters
set smartcase
"Set background to dark for more contrast
"set background=dark
"We want color!! We want color!! We want color!!
syntax enable
"This is my own thingy for editing emails that are fritzing
"current paragraph
map <F2> gqip
"current line
map <S-F2> gq}
"whole document
map <C-F2> ggVGgq		

"DOS formatting of IR Packets
map <C-S-F2> <ESC>:%s/\<\(C1\)\> \<\(FF\)\>/\1\r\r\2/g<CR>ggVGgq
"Don't want tabs replaced with spaces
"set noexpandtab
"Incremental search (whatever that means)
set incsearch
"Disable splash screen
"set shortmess=0 "Doesn't seem to work w/ my system 
"Indent C code by default
set cindent
"No need to save on a :next :previous, etc
"Backspace permissions: [0-2] forget which does which
set backspace=2
"Title the term window
set title
"Smoother changes
set ttyfast
"Show uncompleted command
set showcmd
"Show matching parenthesis
set showmatch
"Show current mode
set showmode
"Keep cursor on same column when doing page movement
set nostartofline
"expansion key (I think it's like the bash command completion)
set wildchar=<TAB>
"show line numbers
set number
"show report on changes to file, 0 means all changes
set report=0
"don't beep at me!!
set noerrorbells
" quiet work around: make visual bell first
set vb
"absolute quiet!!
"set vb t_vb=''
" Hide the mouse pointer while typing, from Alan De Smet
set mousehide
" Key focus follows mouse
set mousefocus
"always show the status line
set laststatus=2


set statusline=%([%q%H%M%R%W]%)
" break the line if too long, then print the relative (to CWD) filename
set statusline+=%<%f\ %y%([%{&ff}][%n]%)
" from https://github.com/scrooloose/vimfiles/blob/master/vimrc
"set statusline+=%{StatuslineTrailingSpaceWarning()}
"display a warning if &paste is set
set statusline+=%#error#
set statusline+=%{&paste?'[paste]':''}
set statusline+=%*

" left/right divide
set statusline+=%=\ ascii:%3b/0x%02B\ c:%c%V\ l:%l/%L\ %p%%%([%q%H%M%R%W]%)



" from https://github.com/scrooloose/vimfiles/blob/master/vimrc
"return '[\s]' if trailing white space is detected
"return '' otherwise
function! StatuslineTrailingSpaceWarning()
	if getfsize(expand("%:p")) < 10000000
    if !exists("b:statusline_trailing_space_warning")

        if !&modifiable
            let b:statusline_trailing_space_warning = ''
            return b:statusline_trailing_space_warning
        endif

        if search('\s\+$', 'nw') != 0
            let b:statusline_trailing_space_warning = '[\s]'
        else
            let b:statusline_trailing_space_warning = ''
        endif
    endif
    return b:statusline_trailing_space_warning
	endif
endfunction


" cursor can go anywhere only in visual mode
set virtualedit=block

"don't want any backup files
set nobackup
set nowritebackup 

"tab handling
"Spread tabs to spaces
"set expandtab

"how many spaces per tab
set tabstop=4
"how many spaces for indentation
set shiftwidth=4
"Treat spaces as tabs? use == for instance
set smarttab
"From  Dirk Ruediger <druediger at gmx.net> on vim.org, tip_id=12
"Highlight hidden characters
set list listchars=tab:��,trail:�,nbsp:�
"From rausse at hotmail.com on vim.org, tip_id=12
"makes spaces feel like real tabs
set softtabstop=4

filetype plugin indent on " load filetype plugins
"default file types configurations
"au BufNewFile *.tex :r ~/headers/tex
if !exists("autocommands_loaded")
	let autocommands_loaded = 1

au BufNewFile,BufRead *.txt set ft=txt
au FileType tex		setlocal tw=80 nocin enc=utf-8 fenc=utf-8 expandtab
au FileType c		set tw=80 expandtab comments=sl:/*,mb:*,elx:*/ softtabstop=4
" doxygen comment magic from ...
au FileType c++,cpp		set tw=80 expandtab com-=:// com+=://! com+=:/// com+=://
au FileType c++,cpp		let b:browsefilter="CPP\t*.cpp;*.c++;*.cxx;*.h;*.hpp;*.h++;*.ipp\nAll Files\t*.*\n"
au FileType cs		set nolist
au FileType make	setlocal nolist noexpandtab
au FileType asm		setlocal nolist noexpandtab
au FileType ls		setlocal nolist noexpandtab
au FileType vim     setlocal nolist noexpandtab
au FileType txt		set nocin noexpandtab nolist tw=80 encoding=utf-8
au FileType eml		set nocin noexpandtab nolist tw=74
au FileType html	setlocal noexpandtab nolist
au FileType xhtml	setlocal noexpandtab nolist
au FileType xml		setlocal expandtab nolist
au FileType sh		setlocal nolist
"au FileType text	setlocal tw=80
"au FileType text	setlocal nocin
"au FileType changelog setlocal nolist
au FileType changelog setlocal expandtab
"From Luc Hermitte
"au BufNewFile,BufRead *.h set ft=c
au BufNewFile,BufRead *.h set ft=cpp

au FileType vim setlocal nocin
au BufRead,BufNewFile *.s43 set ft=msp
au BufRead,BufNewFile *.csproj set expandtab
au FileType java setlocal noexpandtab nolist
au FileType ruby setlocal tw=80 expandtab fdl=1
au FileType svg setlocal encoding=utf-8
au BufRead,BufNewFile *.gv setfiletype dot
au FileType dot setlocal expandtab

" from https://github.com/scrooloose/vimfiles/blob/master/vimrc Sept 4, 2013
"recalculate the trailing whitespace warning when idle, and after saving
autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning
endif


"key word substitutions
"want to keep things somewhat readable on smaller screens...
"imap Yrlwq 012345678901234567890123456789012345678901234567890123456789012345678901234567890<CR>0         1         2         3         4         5         6         7         8<CR>
"some nifty LaTeX partial macros/substitutions
imap =enm \begin{enumerate}<CR>\item<CR>\end{enumerate}<Esc>$ka<Space>
imap =inm \begin{itemize}<CR>\item<CR>\end{itemize}<Esc>$ka<Space>
imap =eqa \begin{equation}<CR>\begin{aligned}<CR><CR>\end{aligned}<CR>\end{equation}<Esc>$kka
imap =eqa* \begin{equation*}<CR>\begin{aligned}<CR><CR>\end{aligned}<CR>\end{equation*}<Esc>$kka
imap =equ* \begin{equation*}<CR><CR>\end{equation*}<Esc>$ka
imap =equ \begin{equation}<CR><CR>\end{equation}<Esc>$ka


"quick insert
au FileType tex imap <F3> \item<Esc>$a<Space>

imap <F9> <Esc>:r $VIMRUNTIME/../fun_header.c<CR>/<date><CR>:s/<date>/\=strftime("%m\/%d\/%y")/<CR>?Title<CR>A
"next might be useful in some situations
"minimize this window in Windows (blah)
map <F7>   <Esc>:stop<CR>
map <F6>   <Esc>:split<CR>
nmap <F6> :split<CR>
map <S-F6> <Esc>:close<CR>
nmap <S-F6> :close<CR>



"hopefully these will work for subst
ab bvr \begin{verbatim}
ab evr \end{verbatim}
ab bmt \begin{displaymath}
ab emt \end{displaymath}

"Give vim a map for keywords: look for a tags file recursively up
set tags=./tags;

if has("gui_running")
	colorscheme darkblue
	" darkblue color scheme doesn't (didn't) have decent support for 
	" tab completes, these colors seem to fit better
	hi Pmenu guifg=#c0c0c0 guibg=#404080
	hi PmenuSel guifg=#c0c0c0 guibg=#2050d0
	hi PmenuSbar guifg=blue guibg=darkgray
	hi PmenuThumb guifg=#c0c0c0
else
	set t_Co=256
	set background=dark
	let g:zenburn_high_Contrast=1
	:colorscheme zenburn
endif

" set a sane path to search with gf: current directory down first,
" then current directory up
set path+=./**,./;

"mappings for dvorak keyboard layout
"beginning with home row movement
"noremap d h
"map h j
"map t k
"map n l
"noremap . e
"noremap e d
"noremap , w

"Session settings
set sessionoptions=blank,buffers,curdir,folds,tabpages


"verse number insert
"thanks to John R. Aldridge, Jr. for his inc and dec script (BlockWork) which 
"served as impetus
"works for only one
"map <F4> :s/\([0-9]\)/\\vr{\1}<CR> 
"\(...\) is considered the \1
"oops have to put a slash infront of + to get more than one digit
"g will match more than once in a line!!
"\\ escapes the slash
map <F4> :s/\([0-9]\+\)/\\vr{\1}/g<CR> 
map <S-F4> :%s/\\r\\n/ /g<CR>
map <F5> :'<,'>s/^/\/\//<CR> 
map <S-F5> :'<,'>s/^/2x/ 
" wipe out xml/html tags
map <F8> :s/<.\{-}>//g<CR>
map <S-F8> :s/^.\{-}\d\+:\(\d\+\): /\\vr{\1}/g<CR>

"win32 cygwin shell setting
"set shell=c:\cygwin\cygwin.bat
"set shell=c:\cygwin\bin\bash\ --login\ -i

"nnoremap <silent> <F3> :Grep<CR>
"let Grep_Cygwin_Find = 1


"grep declaration
"set grepprg=/cgywin/usr/bin/grep\ -nHi\ -B\ s4
"window width ?
"set winwidth=85

"From Luc Hermitte http://hermitte.free.fr/vim/ressources/dollar_VIM/
set magic             " Use some magic in search patterns?  Certainly!

"Insert current date
"imap <F10> <Esc>=strftime("%m\/%d\/%y")<CR>A
"imap <F10> <C-R>=strftime("%m\/%d\/%y")<CR>
imap <F10> <C-R>=strftime("%Y%m%d_%H%M%S00")<CR>

"C coding structure pointer access
imap <F12> ->


"don't highlight the tabs/spaces at the beginning of the line
let g:changelog_spacing_errors = 0

" printdialog plugin options
let g:prd_syntaxList = "no,current,default,print_bw,zellner"
let g:prd_syntaxIdx  = 4
"let g:prd_prtDeviceList = "9040LaserJet"
"let g:prd_prtDeviceList = "LaserJet-9040"
let g:prd_prtDeviceList = "a3861925"
" set letter paper
let g:prd_paperIdx = 7
" set left/right margin to 5mm
let g:prd_leftIdx = 1
let g:prd_rightIdx = 1
let g:prd_topIdx = 1
let g:prd_bottomIdx = 1

"Set printer font
set pfn=Courier_New:h10

" current line highlight
"set cul

" spell checker
"set spell
" type z=  to see suggestions

" mouse model 
set mousemodel=popup

" Make GUI file Open use current directory
set browsedir=buffer

if has("gui_running")
	set guioptions+=pcaA
	" remove toolbar
	set guioptions-=T
	" and tear off menu items, yes it has to be on a separate line
	set guioptions-=t
	if has("gui_gtk2")
"	if has("x11")
		"set guifont=-*-proggycleanszbp-medium-r-normal-*-13-*-*-*-*-*-*-*
		"set guifont=Bitstream\ Vera\ Sans\ Mono\ 11
		"if &columns < 110
		"endif
		set guifont=DejaVu\ Sans\ Mono\ 14
		"set guifont=7x13
"	else
" set guifont=Courier_New:h11:cDefault
"	endif
	endif

	if has("win32")
		"set guifont=DejaVu_Sans_Mono:h12
		set guifont=Consolas:h14
	endif
endif

" highlight searches automatically
set hls
" quick set nolist for files that I have not written
map <C-S-F12> <ESC>:set nolist<CR>

" look for a [ not preceded by a C1 (any chars) and a new line
map <C-F11> /\(C1.*\n\)\@<![<CR>
" delete that line with the bracket when it's not preceded by ... and join the two lines
map <C-S-F11> /\(C1.*\n\)\@<![<CR>ddkJ

" make the splitters between windows be blank
"set fillchars=vert:\ ,stl:\ ,stlnc:\
" before switching buffers, consider files in other tabs and then check for
" other windows
set switchbuf=usetab,useopen
" write current files before compiling
set autowrite


vnoremap <C-S-B> y :r ! diatheke -b ESV -f OSIS -k <C-R>"<CR>
set lz " do not redraw while running macros
set so=0 " Keep 0 lines (top/bottom) for scope


" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
if !exists("DiffOrig")
	command! DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
			\ | wincmd p | diffthis
endif

map <F3> <ESC>:cnext<CR>
map <S-F3> <ESC>:cprev<CR>

" don't need to input other types of languages
set imdisable

" load COSMIC listing files as assembly files
au BufRead,BufNewFile *.ls	set filetype=asm
" emerge vim recommended set these, I get to look them up later

" Customize Status line color
" highlight StatusLine guifg=SlateBlue guibg=Yellow
" highlight StatusLineNC guifg=Gray guibg=White
set hl+=sr

" ensure that all CMake files get loaded as CMake instead of simple plain text
au BufRead,BufNewFile CMake*.txt set filetype=cmake expandtab list

map <S-F9> :s/^/\/\/<CR>
map <C-S-F9> :s/^\/\//<CR>

"set directory+="c:\Temp\\"

" remove the current directory from the list
"set directory-=.
" clear the available directories
set directory=
" add another of my own temp to the list
" win64
if has("win32")
	set directory+=~/_vim/tmp//
elseif has("win64")
	set directory+=~/_vim/tmp//
else
	set directory+=~/.vim/tmp//
endif
" add the file's directory back at the end of the list
set directory+=.

" start nerd tree
noremap ,n :NERDTreeToggle<CR>
" use NERD tree instead of netrw
let NERDTreeHijackNetrw=1

let g:zenburn_high_Contrast=1

" experimental json support
au! BufRead,BufNewFile *.json setfiletype json

" set syntax space errors, from: http://vim.wikia.com/wiki/Highlight_unwanted_spaces
let c_space_errors = 1

" fold the current scope
nmap ,z Vj%zf<CR>
nmap <C-Up> z^
nmap <C-Down> z+

" can just hide buffers, don't have to write them out before switching
set hidden
